import styled from "styled-components";

export const SectionServices  = styled.div`
    padding: 60px 0;
`

export const SectionContainer  = styled.div`

`

export const CardsList = styled.div `
    margin: 30px 0 0;
    display: flex;
    justify-content: space-around;
    align-items: center;
    gap: 20px;
`

export const CardItemTitle = styled.div `
    color: #fff;
    font-size: 1rem;
    font-weight: 500;
    padding: 0 50px;
    text-align: center;
    margin: 20px 0 0;
`

export const Title  = styled.div`
    font-size: 1rem;
    font-weight: 600;
    color: #000;
    text-align: center;
    text-transform: uppercase;
`
export const SubTitle  = styled.div`
    font-size: 1.75rem;
    font-weight: 900;
    color: ${(props) => props.theme.colors.secondary};
    text-align: center;
    text-transform: uppercase;
    padding: 0 320px;
    margin: 12px 0;
    line-height: 1;
`

export const Description = styled.div `
    text-align: center;
    font-size: .875rem;
    font-weight: 500;
`