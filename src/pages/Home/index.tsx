import { CardsList, Description, SectionContainer, SectionServices, SubTitle, Title } from "./styled";
import { ServiceCards } from '../../components/ServiceCards'

import ImgOuts from '../../assets/images/img-outs.png'
import ImgMobile from '../../assets/images/img-mobile.png'
import ImgSoft from '../../assets/images/img-soft.png'
import ImgTec from '../../assets/images/img-tec.png'

export function Home(){
    return(
        <SectionServices>
            <SectionContainer className="container">
                <Title>Nossos Serviços</Title>
                <SubTitle>Mais de 20 anos trazendo inovação e tecnologia</SubTitle>
                <Description>
                    A Avanz atua com tecnologia, consultoria e desenvolvimento de software, qualquer que seja o seu segmento.
                </Description>
                <CardsList>
                    <CardsList>
                        <ServiceCards
                            image={ImgOuts}
                            title="OUTSOURCING DE PROFISSIONAIS DE TI"
                        />
                        <ServiceCards
                            image={ImgMobile}
                            title="DESENVOLVIMENTO DE APLICATIVOS MOBILE"
                        />
                        <ServiceCards
                            image={ImgSoft}
                            title="DESENVOLVIMENTO DE SOFTWARE SOB MEDIDA"
                        />
                        <ServiceCards
                            image={ImgTec}
                            title="CONSULTORIA EM TECNOLOGIA"
                        />
                    </CardsList>
                </CardsList>

            </SectionContainer>
        </SectionServices>
    );
}