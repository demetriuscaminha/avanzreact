import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle `
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }
    body {
        font-family: ${(props) => props.theme.font.family};
        color: ${(props) => props.theme.font.color};
        font-weight: ${(pros) => pros.theme.font.fontweight}
    }
    dl, ol, ul {
        margin-top: 0;
        margin-bottom: 0;
    }
    .uptext{
        text-transform: uppercase;
    }
`;