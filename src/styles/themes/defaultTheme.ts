export const defaultTheme =  {
  font: {
    family: "'Montserrat', sans-serif",
    color: 'black',
    fontweight: 400
  },
  colors: {
    primary: '#FFA300',
    secondary: '#3D3A35',
    tertiary: '#FAE5C3',
    alert: '#F05921',
    success: '#00C9AE',
    border: '#E5E5E5',
    textColor: '#3D3A35',
    textGray: '#5F5F5F',
    darkGrey: '#1F1F1E',
    lightGray: '#F6F6F6',
    footer: '#E0E0E0',
    social: '#868686',
  },
} as const