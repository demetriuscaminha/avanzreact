import { Routes, Route, BrowserRouter } from 'react-router-dom'
import { Layout } from './components/Layout'
import { Home } from './pages/Home'
import { QuemSomos } from './pages/quemsomos'
import { Solucoes } from './pages/Solucoes'
import { Clientes } from './pages/Clientes'

export function Router() {
    return (
        <BrowserRouter>
            <Routes>
                <Route path="/" element={<Layout />}>
                    <Route path="/" element={<Home />}/>
                    <Route path="/quemsomos" element={<QuemSomos />}/>
                    <Route path="/Solucoes" element={<Solucoes />}/>
                    <Route path="/rededeatendimento" element={<Clientes />}/>
                </Route>
            </Routes>
        </BrowserRouter>
    )
}