import {
    HeaderContainer,
    HeaderLogo,
    HeaderWrapper,
    MainNav
} from './styles'

import LogoImage from '../../../assets/images/logo-avanz.svg'
import imageBanner from '../../../assets/images/main-banner.png'
import { Link } from 'react-router-dom'
import { Carousel } from 'react-responsive-carousel'
import 'react-responsive-carousel/lib/styles/carousel.min.css'
import '../../../assets/css/bootstrap.min.css'


export function Header() {
    return(
    <HeaderWrapper className='header-wrapper'>
        <Carousel showThumbs={false} showStatus={false}>
            {[
                <img
                    key="banner1"
                    src={imageBanner}
                    alt="banner hapvida"
                    loading="lazy"
                />,
                <img
                    key="banner2"
                    src={imageBanner}
                    alt="banner hapvida"
                    loading="lazy"
                />
            ]}
        </Carousel>
        <HeaderContainer className='header-nav'>
          <div className='container d-flex justify-content-between'>
            <HeaderLogo src={LogoImage} />
            <MainNav>
              <ul className='main-nav d-flex justify-content-end uptext'>
                <li>
                  <Link to="./">Início</Link>
                </li>
                <li>
                  <Link to="/quemsomos">Quem Somos</Link>
                </li>
                <li>
                  <Link to="/solucoes">Soluções</Link>
                </li>
                <li>
                  <Link to="/clientes">Clientes</Link>
                </li>
                <li>
                  <Link to="/blog">Blog</Link>
                </li>
                <li>
                  <Link to="/contato">Contato</Link>
                </li>
              </ul>
            </MainNav>
          </div>
        </HeaderContainer>
    </HeaderWrapper>
    )
}