import styled from 'styled-components'

export const HeaderWrapper = styled.header`
`
export const HeaderContainer = styled.div`
  padding: 0;
  background-color: transparent;
  position: absolute;
  top: 40px;
  left: 0;
  right: 0;
  z-index: 1;
`
export const HeaderLogo = styled.img`
    max-width: 100%;
    height: auto;
`

export const MainNav = styled.div`
  width: 100%;
  ul {
    display: flex;
    list-style-type: none;
    justify-content: space-around;
    li{
      a {
        color: white;
        font-size: 14px;
        font-weight: 400;
        text-decoration: none;
        padding: 0 20px;
        &:hover {
          color: ${(props) => props.theme.colors.primary};
        }
      }
    }
  }
`

export const bannerWrap = styled.div`
  
`