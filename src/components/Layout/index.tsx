import { Header } from "./Header";
import { Outlet } from "react-router-dom";
import { LayoutContainer } from "./styles";



export function Layout(){
    return(
        <LayoutContainer>
            <Header />
            <Outlet />
            <h1>Footer</h1>
        </LayoutContainer>
    )
}