import styled from "styled-components";

export const SectionServices  = styled.div`
`

export const InformationContainer = styled.div`
`

export const CardItem = styled.div `
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    background: ${(props) => props.theme.colors.secondary};
    height: 290px;
    flex-grow: 1;
    flex-shrink: 1;
    flex-basis: 0;
`

export const Title = styled.div`
    color: #fff;
    font-size: 1rem;
    font-weight: 500;
    padding: 0 50px;
    text-align: center;
    margin: 20px 0 0;
`
