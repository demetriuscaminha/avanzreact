import { SectionServices, Title, CardItem } from "./styles";

interface ServiceCardsProps {
    image: string
    title: string
}

export function ServiceCards({
    image,
    title,
}: ServiceCardsProps) {
    return (
        <CardItem>
            <img src={image} alt="" />
            <Title>{title}</Title>
        </CardItem>
    )
}